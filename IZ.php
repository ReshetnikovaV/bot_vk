<?php

include "IZ_functions.php";
include "IZ_speak.php";
require_once('simplevk-master/autoload.php');
use DigitalStar\vk_api\VK_api as vk_api; // библиотека для vk
require  './vendor/autoload.php';
use Krugozor\Database\Mysql\Mysql as Mysql; // библиотека для БД

//// ИНФОРМАЦИЯ ДЛЯ ПОДКЛЮЧЕНИЯ
const VK_KEY = "***";
const ACCESS_KEY = "***";
const VERSION = "5.81";

//// ОСНОВНЫЕ ПЕРЕМЕННЫЕ
$vk = vk_api::create(VK_KEY, VERSION)->setConfirm(ACCESS_KEY);
$db = Mysql::create("localhost", "***", "***")->setDatabaseName("***");
$fun = new IZ_functions(VK_KEY, VERSION); 
$voice = new IZ_speak(VK_KEY, VERSION);

$btn_0 = $vk->buttonText('!отмена', 'red', ['command' => 'btn_0']);
$btn_1 = $vk->buttonText('Камень', 'blue', ['command' => 'btn_1']);
$btn_2 = $vk->buttonText('Ножницы', 'green', ['command' => 'btn_2']);
$btn_3 = $vk->buttonText('Бумага', 'white', ['command' => 'btn_3']);
$btn_4 = $vk->buttonText('!играю', 'green', ['command' => 'btn_4']);
$btn_5 = $vk->buttonText('!исключить', 'red', ['command' => 'btn_5']);
$btn_6 = $vk->buttonText('!нет', 'white', ['command' => 'btn_6']);
$btn_7 = $vk->buttonText('!привет', 'blue', ['command' => 'btn_7']);

$vk->initVars($peer_id, $message, $payload, $vk_id, $type, $data);
$messageLow = mb_strtolower($message);
$members = $vk->request('messages.getConversationMembers', ['peer_id' => $peer_id])['items'];
$accounts = $vk->request('messages.getConversationMembers', ['peer_id' => $peer_id])['profiles'];

if ($vk->request('messages.getConversationMembers', ['peer_id' => $peer_id])['count'] > 2) // получение короткого id чата для проверки на беседу или пользователя
    $chat_id = $peer_id - 2000000000;
else $chat_id = 0;

foreach ($members as $k1 => $v1) { // проверка на администратора
    foreach ($members[$k1] as $k2 => $v2) {
        if ($k2 == "is_admin" && $v2 == 1)
            $adm_list .= $members[$k1][member_id]. " ";
    }
}
$list = explode(" ", $adm_list);
foreach ($list as $adm_id) {
    if ($adm_id == $vk_id)
        $is_admin += 1;
    if ($adm_id > 0)
        $adm_str .= " @id". $adm_id. ",";
}
if ($is_admin != 1)
    $is_admin = 0;

foreach ($accounts as $k1 => $v1) { // получение имени $n для регистрации в БД
    foreach ($accounts[$k1] as $k2 => $v2) {
        if ($accounts[$k1][id] == $vk_id)
            $n = $accounts[$k1][first_name];
    }
}
////

if ($data->type == 'message_new') { // проверка на новое сообщение
    if (isset($data->object->payload)) // проверка на кнопки
        $payload = json_decode($data->object->payload, True);
    else
        $payload = null;
    $payload = $payload['command'];

//// КНОПКИ

    if ($payload == 'btn_0') {
        $vk_id1 = $db->query("SELECT * FROM knb ORDER BY id DESC LIMIT 1")->fetch_assoc()['first'];
        $vk_id2 = $db->query("SELECT * FROM knb ORDER BY id DESC LIMIT 1")->fetch_assoc()['second'];
        $chat = $db->query("SELECT * FROM knb WHERE id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->fetch_assoc()['chat_id'];
        if ($vk_id == $vk_id1 || $vk_id == $vk_id2 || $is_admin) {
            $vk->sendButton($peer_id, "Ок", [[]]);
            $vk->sendButton($chat, "Игра была отменена", [[]]);
            if ($vk_id1) $vk->sendButton($vk_id1, "Игра была отменена", [[]]);
            if ($vk_id2) $vk->sendButton($vk_id2, "Игра была отменена", [[]]);
            $db->query("DELETE FROM knb ORDER BY id DESC LIMIT 1");
        }
        else 
            $vk->sendMessage($peer_id, "Ты не можешь отменить эту игру!");
    }

    if ($payload == 'btn_1' || $payload == 'btn_2' || $payload == 'btn_3') {
        if ($payload == 'btn_1') $step = "Камень";
        if ($payload == 'btn_2') $step = "Ножницы";
        if ($payload == 'btn_3') $step = "Бумага";
        $vk->sendButton($peer_id, "Ход сделан!", [[]]);
        $db->query("UPDATE knb SET step1 = '$step' WHERE first = $vk_id");
        $db->query("UPDATE knb SET step2 = '$step' WHERE second = $vk_id");
        $done = $db->query("SELECT * FROM knb WHERE NOT step1 = '_' AND NOT step2 = '_' AND id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->getNumRows();
        if ($done) {
            $f = $db->query("SELECT * FROM knb WHERE id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->fetch_assoc()['step1'];
            $s = $db->query("SELECT * FROM knb WHERE id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->fetch_assoc()['step2'];
            $chat = $db->query("SELECT * FROM knb WHERE id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->fetch_assoc()['chat_id'];
            $win = $fun->knb($f, $s);
            if ($win == "Ничья")
                $vk->sendButton($chat, "$f vs $s\n\n$win!", [[]]);
            else {
                $winer = $db->query("SELECT * FROM knb WHERE id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->fetch_assoc()[$win];
                $winer_nick = $db->query("SELECT * FROM IZ WHERE vk_id = $winer AND chat_id = $chat")->fetch_assoc()['nick'];
                $score = $db->query("SELECT * FROM IZ WHERE vk_id = $winer AND chat_id = $chat")->fetch_assoc()['wins_knb'];
                $score++;
                $db->query("UPDATE IZ SET wins_knb = $score WHERE vk_id = $winer AND chat_id = $chat");
                if ($winer_nick)
                    $vk->sendButton($chat, "$f vs $s\n\nПобедитель -- $winer_nick!", [[]]);
                else $vk->sendButton($chat, "$f vs $s\n\nПобедитель -- $win!", [[]]);
            }
        }
    }

    if ($payload == 'btn_4') {
        if ($vk->sendMessage($vk_id, "Ждём противника!")) {
            $db->query("UPDATE knb SET first = $vk_id WHERE first = 0");
            $player1 = $db->query("SELECT * from knb WHERE first = $vk_id AND second = 0")->getNumRows();
            if (!$player1)
                $db->query("UPDATE knb SET second = $vk_id WHERE NOT first = 0 AND second = 0");
            else $vk->sendMessage($peer_id, "Нужен ещё один игрок!");
            $player2 = $db->query("SELECT * FROM knb WHERE NOT second = 0 AND id = (SELECT id FROM knb ORDER BY id DESC LIMIT 1)")->getNumRows();
            if ($player2) {
                $vk->sendButton($peer_id, "Битва началась!", [[$btn_0]]);
                $vk_id1 = $db->query("SELECT * FROM knb ORDER BY id DESC LIMIT 1")->fetch_assoc()['first'];
                $vk->sendButton($vk_id1, "Делай ход!", [[$btn_1, $btn_2, $btn_3, $btn_0]]);
                $vk_id2 = $db->query("SELECT * FROM knb ORDER BY id DESC LIMIT 1")->fetch_assoc()['second'];
                $vk->sendButton($vk_id2, "Делай ход!", [[$btn_1, $btn_2, $btn_3, $btn_0]]);
            }
        }
    }

    if ($message == '!играть' && $chat_id != 0) { // начать игру в КНБ
        $db->query("INSERT INTO knb (first, second, step1, step2, chat_id) VALUES (0, 0, '_', '_', $peer_id)");
        $vk->sendButton($peer_id, "Я не смогу добавить тебя в игру, если ты не разрешишь мне отправлять тебе личные сообщения! Это нужно только один раз! Для этого напиши мне что-нибудь -- https://vk.com/im?sel=-190062839\n\nУже написал(а)?", [[$btn_4, $btn_0]]);
    }

    if ($data->object->action->type == "chat_kick_user" && $data->object->action->member_id == $data->object->from_id) {
        $kick_id = $data->object->from_id;
        $db->query("INSERT INTO gone (vk_id, chat_id) VALUES ($kick_id, $peer_id)");
        $vk->sendButton($peer_id, "$adm_str исключить?", [[$btn_5, $btn_6]]);
    }

    if ($payload == 'btn_5') {
        if ($is_admin) {
            $kick_id = $db->query("SELECT * FROM gone ORDER BY id DESC LIMIT 1")->fetch_assoc()['vk_id'];
            $vk->request('messages.removeChatUser', ['chat_id' => $chat_id, 'user_id' => $kick_id]);   
            $vk->sendButton($peer_id, "Готово", [[]]);
            $db->query("DELETE FROM gone ORDER BY id DESC LIMIT 1");
        }
        else
            $vk->sendMessage($peer_id, "У тебя нет доступа к этой команде!");
    }

    if (mb_substr($message, 0, 5) == '!искл') { // исключить пользователя, даже если он сам покинул чат
        if ($is_admin) {
            $kick_id = mb_substr($message, 6);
            $kick_id = explode("|", mb_substr($kick_id, 3))[0];
            if ($kick_id == "")
                $vk->sendMessage($peer_id, "Ты не указал id пользователя!");
            else
                $vk->request('messages.removeChatUser', ['chat_id' => $chat_id, 'user_id' => $kick_id]);  
        }
        else
            $vk->sendMessage($peer_id, "У тебя нет доступа к этой команде!");
    }

    if ($payload == 'btn_6' || $data->object->action->type == "chat_invite_user" && $data->object->action->member_id == $db->query("SELECT * FROM gone ORDER BY id DESC LIMIT 1")->fetch_assoc()['vk_id']) {
        $vk->sendButton($peer_id, "Ок", [[]]);
        $db->query("DELETE FROM gone ORDER BY id DESC LIMIT 1");
    }

//// ОБЫЧНЫЕ КОМАНДЫ

    if (mb_substr($message,0,7) == '!привет' || $payload == 'btn_7') { // регистрация в БД
        $sql = $db->query("SELECT * from IZ WHERE vk_id = $vk_id AND chat_id = $peer_id")->getNumRows();
        if ($sql) {
            $vk->sendButton($peer_id, "Мы уже знакомы!", [[]]);
        } else {
            $db->query("INSERT INTO IZ (vk_id, chat_id) VALUES ($vk_id, $peer_id)");
            $db->query("UPDATE IZ SET nick = '$n' WHERE id=LAST_INSERT_ID()");
            $vk->sendButton($peer_id, "Привет, $n!", [[]]);
        }
    }

    if (mb_substr($message,0,4) == '!ник') { // установить себе ник
        $sql = $db->query("SELECT * from IZ WHERE vk_id = $vk_id AND chat_id = $peer_id")->getNumRows();
        if ($sql) {
            $user_nick = mb_substr($message ,5);
            $db->query("UPDATE IZ SET nick = '$user_nick' WHERE vk_id = $vk_id AND chat_id = $peer_id");
            $name = $db->query("SELECT * FROM IZ WHERE vk_id=$vk_id AND chat_id = $peer_id")->fetch_assoc()['nick'];
            $vk->sendMessage($peer_id, "Теперь буду звать тебя $name!");
        } else
            $vk->sendButton($peer_id, "Сначала поздоровайся!", [[$btn_7]]);
    }

    if (strpos($messageLow, '!моё имя') !== false || strpos($messageLow, '!мое имя') !== false) { // узнать свой ник
        $name = $db->query("SELECT * FROM IZ WHERE vk_id=$vk_id AND chat_id = $peer_id")->fetch_assoc()['nick'];
        $vk->sendMessage($peer_id, "Тебя зовут $name!");
    }

    if (strpos($messageLow, '!двойной рандом до') !== false) { // получить рандомные числа
        $answer = "Твои рандомные числа — ". $fun->randomizeTwo($message). "!";
        $vk->sendMessage($peer_id, $answer);
    } else if (strpos($messageLow, '!рандом до') !== false) {
        $answer = "Твоё рандомное число — ". $fun->randomizeOne($message). "!";
        $vk->sendMessage($peer_id, $answer);
    }

    if (strpos($messageLow, '!подбрось монетку') !== false) { // подбросить монетку
        if (rand() % 2 == 0)    $answer = "Выпал орёл!";
        else                    $answer = "Выпала решка!";
        $vk->sendMessage($peer_id, $answer);
    }

    if (mb_substr($message,0,6) == '!скажи') { // повторить предложение от лица бота
        $answer = $voice->say($message);
        $vk->sendMessage($peer_id, $answer);
    }
    
    if (strpos($messageLow, '!табл') !== false && $chat_id != 0) { // показать очки игроков в кнб за всё время
        $table_ids = $db->query("SELECT * FROM IZ ORDER BY id DESC LIMIT 1")->fetch_assoc()['id'];
        $tabl = "Количество побед в КНБ:\n";
        for ($i = 1; $i <= $table_ids; $i++) {
            $nik = $db->query("SELECT * FROM IZ WHERE id = $i AND chat_id = $peer_id")->fetch_assoc()['nick'];
            $id_ = $db->query("SELECT * FROM IZ WHERE id = $i AND chat_id = $peer_id")->fetch_assoc()['vk_id'];
            $player = "@id$id_ ($nik)";
            $points = $db->query("SELECT * FROM IZ WHERE id = $i AND chat_id = $peer_id")->fetch_assoc()['wins_knb'];
            if ($points)
                $players[$player] = $points;
        }
        arsort($players);
        foreach ($players as $k => $v) {
            $tabl .= "\n$k -- $v";
        }
        $vk->sendMessage($peer_id, $tabl);
    }
}
